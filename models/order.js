const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type: String,
		required: [true, "User ID is required"]
	},
	products : [
	{
		productId : {
			type: String
		},
		quantity : {
			type: Number
		},
		price: {
			type: Number
		},
		totalPrice : {
			type: Number
		}
	}],
	totalAmount : {
			type: Number,
		},
	purchaseDate : {
			type: Date
			//default: new Date()
		}
})

module.exports = mongoose.model("order", orderSchema)